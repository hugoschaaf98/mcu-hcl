# [MCU-HCL] Cahier des charges

Cahier des charges pour le développement de la *mcu-hcl* : une bibliothèque générique de contrôle des périphériques de microcontrôleurs.

--

## Glossaire

MCU : microcontrôleur
HCL : Hardware Control Library (Bibliothèque de contrôle du matériel)
STL : Standart Template Library (Bibliothèque standard C++)
Heap : mémoire allouée dynamiquement
Driver : code permettant d'intéragir avec un système numérique extérieur.

--

## Spécifications

La *mcu-hcl* doit rendre générique la configuration et l'utilisation des périphériques d'un microcontrôleur afin de permettre aux développeur.se.s de concervoir des applications, drivers etc... de manière générique et maintenable. C .

- Language :
  - C++11
  - C (**réservé à la réutilisation de code existant** : la *mcu-hcl* peut de baser sur des drivers existants écrits en C)

- Mémoire :
  - pas d'allocation dynamique de mémoire

- Architectures :
  - ARM-Cortex et premier lieu
  - Plus si affinités

- Microcontrôleurs :
  - *Nordic nRF52840* et *STM32F303K8* en premier lieu
  - Le plus possible ensuite !
