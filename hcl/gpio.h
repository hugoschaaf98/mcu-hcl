/**
 * @file gpio.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef GPIO_H
#define GPIO_H

#include <cstdint>
#include <cstdbool>

namespace hcl {

	class Gpio {
	
	public :

		enum class PinMode : std::uint8_t {
			Input,		//! Input pin
			Output,		//! Output pin
			None=0xFF 	//! Eg. if not connected
		};

		enum class DriveMode : std::uint16_t {
			IPullup 	= 1,	//! Input Pullup
			IPulldown 	= (1<<1),	//! Input PullDown
			IHighZ		= (1<<3), 	//! Input High Impedance 
			IOHighDrive = (1<<2), 	//! Typical output drive mode. Can source/sink more current
			OOpenDrainDriveLow 	= (1<<4),	//! 1 -> high Z, 		0 -> current sink (strong 0)
			OOpenDrainDriveHigh = (1<<5),	//! 1 -> current sink	0 -> high Z
			None	= 0xFFFF 	//! Eg. if not connected
		};

		// Constructors
		Gpio(PinMode pmode, DriveMode dmode, bool isHigh): pinMode_{pmode}, driveMode_{dmode}, isHigh_{isHigh} 
		{}
		Gpio(): Gpio{PinMode::None, DriveMode::None, false}
		{}

		// Public Getters
		PinMode getPinMode() const { return pinMode_; }
		DriveMode getDriveMode() const { return driveMode_; }

		// Others

		/**
		 * @brief Set Gpio high
		 * 
		 */
		void set() { write(true); }

		/**
		 * @brief Set Gpio low
		 * 
		 */
		void reset() { write(false); }
		
		/**
		 * @brief 	Toggles the Gpio (when set as an output).
		 * @details	This is useful in times when you want to do things such as flash an LED. Calls the Write() method
		 */
		void toggle() { write(!getState()); }

		/**
		 * @brief Set the pin mode
		 * 
		 * @param pmode  @see Gpio::PinMode
		 * 
		 */
		void setPinMode(PinMode pmode)
		{ 
			if( portSetPinMode(pmode) ) {
				pinMode_ = pmode;
			}
			else {
				pinMode_ = PinMode::None;
			}
		}

		
		/**
		 * @brief Set the drive mode
		 * 
		 * @param dmode @see Gpio::DriveMode
		 */
		void setDriveMode(DriveMode dmode)
		{
			if( portSetDriveMode(dmode) ) {
				driveMode_ = dmode;
			}
			else {
				driveMode_ = DriveMode::None;
			}
		}

		/**
		 * @brief	Setup pin and drive modes
		 * 
		 * @details Calls setPinMode(), then setDriveMode()
		 * 
		 * @param pmode @see Gpio::DriveMode
		 * @param dmode @see Gpio::PinMode
		 */
		void setModes(PinMode pmode, DriveMode dmode)
		{
			setPinMode(pmode);
			setDriveMode(dmode);
		}
		
		/**
		 * @brief Reads the value of the Gpio.
		 * 
		 * @return The state of the Gpio
		 */
		bool read()
		{
			bool s = false;
			s = portReadPin();
			return s;
		}

		/**
		 * @brief Sets the Gpio low or high.
		 * 
		 * @param isHigh The new gpio state 
		 */
		void write(bool isHigh)
		{
			if(portWritePin(isHigh)) { 
				setState(isHigh);
			}
		}

	protected :

		// Those methods shouldn't be accessed by the user to guaranty that values reflect
		// the actual hardware state/settings. This is for data integrity.

		// Getters
		bool getState() const { return isHigh_; } // User should get isHigh_'s value through read() method
		// Setters
		void setState(bool isHigh) { isHigh_ = isHigh; }

		// Platform specific methods

		/**
		 * @brief Set pin mode
		 * 
		 * @param pmode 
		 * @return true  on success 
		 * @return false on failure
		 */
		virtual bool portSetPinMode(PinMode pmode) = 0;

		/**
		 * @brief Set drive mode
		 * 
		 * @param dmode 
		 * @return true on success
		 * @return false on failure
		 */
		virtual bool portSetDriveMode(DriveMode dmode) = 0;

		/**
		 * @brief Read a gpio state
		 * 
		 * @return true if gpio is High
		 * @return false if gpio is Low (or if not connected)
		 */
		virtual bool portReadPin() = 0;

		/**
		 * @brief Write a gpio
		 * 
		 * @param isHigh set Gpio High or Low
		 * @return true on success
		 * @return false on failure
		 */
		virtual bool portWritePin(bool isHigh) = 0;

	 private :

		PinMode pinMode_; //! The GPIO pin mode
		DriveMode driveMode_; //! The GPIO drive mode
		bool isHigh_; //! The GPIO state (High or Low)
	};

} // namespace hal

#endif // GPIO_HAL