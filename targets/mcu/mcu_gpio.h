/**
 * @file mcu_gpio.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef MCU_GPIO_H
#define MCU_GPIO_H

// standart
#include <cstdint>
#include <cstdbool>
#include <iostream>

// HAL 
#include "gpio.h"

// Target specific
#include "mcu_pin_names.h"

namespace hcl
{
namespace mcu 
{

	// - Begin
	// ONLY HERE FOR DEBUGGING PURPOSES AND PROOF OF CONCEPT
	inline
	std::ostream& operator<<(std::ostream& os, hcl::Gpio::PinMode pmode) {
		return os << static_cast<std::uint8_t>(pmode);
	}

	inline
	std::ostream& operator<<(std::ostream& os, hcl::Gpio::DriveMode dmode) {
		return os << static_cast<std::uint16_t>(dmode);
	}
	// - End

	class Gpio: public hcl::Gpio {

	public:

		// Constructors
		Gpio(PinName pin, PinMode pmode, DriveMode dmode, bool isHigh): hcl::Gpio{pmode, dmode, isHigh}, pin_{pin}
		{	
			std::cout<<"~~~ Entering "<<__func__<<"() ~~~\n";
			setModes(pmode, dmode);
			if(getPinMode() == PinMode::Output)
			{
				write(isHigh);
			}
			std::cout<<"Gpio "<<getPinName()<<" setup\n";
			std::cout<<"~~~ Exiting "<<__func__<<"() ~~~\n";
		}
		Gpio(PinName pin, PinMode pmode, DriveMode dmode): Gpio(pin, pmode, dmode, false) {;;}
		Gpio(): Gpio(PinName::NC, PinMode::None, DriveMode::None, false) {;;}

		// Public getters

		/**
		 * @brief Get the Pin Name 
		 * 
		 * @return PinName 
		 */
		PinName getPinName() const { return pin_; }
	
	protected:

		// Platform specific
	
		/**
		 * @brief Set pin mode
		 * 
		 * @param pmode 
		 * @return true  on success 
		 * @return false on failure
		 */
		bool portSetPinMode(PinMode pmode)
		{
			(void) pmode;
			if( getPinName() != PinName::NC ) { 
				std::cout<<"Gpio pin mode :  "<< getPinMode() <<'\n';			
				return true; 
			}
			else { return false; }
		}

		/**
		 * @brief Set drive mode
		 * 
		 * @param dmode 
		 * @return true on success
		 * @return false on failure
		 */
		bool portSetDriveMode(DriveMode dmode)
		{
			(void) dmode;
			if( getPinName() != PinName::NC ) { 
				std::cout<<"Gpio Drive mode : "<< getDriveMode() <<'\n';
				return true;
			}
			else { return false; }
		}


		/**
		 * @brief Read a gpio state
		 * 
		 * @return true if gpio is High
		 * @return false if gpio is Low (or if not connected)
		 */
		bool portReadPin()
		{
			bool s = getState();
			std::cout<<"Gpio state :"<<(s ? "High\n" : "Low\n");
			return s;
		}

		/**
		 * @brief Write a gpio
		 * 
		 * @param isHigh set Gpio High or Low
		 * @return true on success
		 * @return false on failure
		 */
		bool portWritePin(bool isHigh)
		{
			if(getPinMode() == PinMode::Output && getPinName() != PinName::NC) { 
				std::cout<<"New Gpio state : "<<(isHigh ? "High\n" : "Low\n");
				return true;
			}
			else { return false; }
		}

	private:

		PinName pin_;	//! The pin name (platform specific)

	};

} // namespace mcu
} // namespace hal

#endif // MCU_GPIO_H