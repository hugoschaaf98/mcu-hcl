/**
 * @file pinnames.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef PINNAMES_H
#define PINNAMES_H

#include <cstdint>

// Just for testing purposes
#include <iostream>

namespace hcl 
{
namespace mcu 
{

	enum class PinName : std::int32_t {
		
		// Port0
		p0  = 0,
		p1,
		p2,
		p3,
		p4,
		p5,
		p6,
		p7,
		p8,
		p9,
		p10,
		p11,
		p12,
		p13,
		p14,
		p15,
		// etc...
		// Port 1
		p42 = 42,
		p43,
		p44,
		p45,
		p46,
		p47,

		NC = (std::int32_t) 0xFFFFFFFF	// Not Connected
	};

	// - Begin
	// ONLY HERE FOR DEBUGGING PURPOSES AND PROOF OF CONCEPT
	inline
	std::ostream& operator<<(std::ostream& os, PinName p) {
		if(p == PinName::NC) return os <<"NC";
		else return os <<"P."<<static_cast<std::int32_t>(p);
	}
	// - End

} // namespace mcu
} // namespace hal



#endif