/**
 * @file device.h
 * @author your name (you@domain.com)
 * @brief Some dummy functions to test the hal
 * @version 0.1
 * @date 2021-03-22
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef DEVICE_H
#define DEVICE_H

#include "gpio.h"

namespace dev {

	using namespace hcl;

	void setGpioHigh(Gpio& gpio)
	{
		gpio.set();
	}

	void setGpioLow(Gpio& gpio)
	{
		gpio.reset();
	}

} // namespace dev

#endif // DEVICE_H