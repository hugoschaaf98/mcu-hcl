/**
 * @file test.cpp
 * @author your name (you@domain.com)
 * @brief  Test c++ file
 * 
 * @note Compile it using < g++ -Wall -Wextra -Wconversion -std=c++11 -o test test.cpp > 
 * 
 * @version 0.1
 * @date 2021-03-18
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <cstdint>

#include "mcu/mcu_gpio.h"
#include "device.h"

using namespace hcl::mcu;

void gpio0Test()
{
	std::cout<<"\n~~~~~~"<<__func__<<"()~~~~~~\n";

	// Create an output and play with it
	Gpio gpio0{PinName::p0, Gpio::PinMode::Output, Gpio::DriveMode::IPullup, true};
	gpio0.set();
	gpio0.toggle();
	gpio0.read();

	dev::setGpioHigh(gpio0);
}


void gpio1Test()
{
	std::cout<<"\n~~~~~~"<<__func__<<"()~~~~~~\n";

	// Create a default and setup afterwards
	Gpio gpio1;

	gpio1.write(true);
	gpio1.setPinMode(Gpio::PinMode::Input);
	gpio1.read();
}


int main ()
{
	// Gpio gpio; // Error - Gpio class is abstract	
	gpio0Test();
	gpio1Test();

	return 0;
}